﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProductsTest.Repository.DTO;
using ProductsTest.Repository.DTO.Validators;
using ProductsTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProductsTest.Repository.Services
{
    public class ProductService : IProductService
    {
        private readonly ProductTestDbContext dbContext;
        private readonly IMapper mapper;

        public ProductService(ProductTestDbContext dbContext, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.mapper = mapper;
        }

        public async Task<IList<ProductModel>> GetAllAsync()
        {
            var entities = await dbContext.Products
                .Include(e => e.Category)
                .ToArrayAsync();

            return entities
                .Select(e => mapper.Map<ProductModel>(e))
                .ToList();
        }

        public async Task<ProductModel> GetAsync(int id)
        {
            var entity = await dbContext.Products
                .Include(e => e.Category)
                .SingleOrDefaultAsync(e => e.Id == id);

            return mapper.Map<ProductModel>(entity);
        }

        public async Task CreateOrUpdateAsync(ProductModel model)
        {
            var validator = new ProductValidator();
            if (!validator.Validate(model).IsValid)
                throw new ValidationException("Validation error");

            var entity = await dbContext.Products.FindAsync(model.Id);
            var categoryEntity = await dbContext.Categories.FindAsync(model.Category.Id);

            if (entity == null)
            {
                entity = new Product();
                await dbContext.Products.AddAsync(entity);
            }

            if (categoryEntity == null || categoryEntity.Name != model.Category.Name)
            {
                var byNameQuery = dbContext.Categories
                    .FirstOrDefault(e => e.Name == model.Category.Name);

                if (byNameQuery == null)
                {
                    categoryEntity = new Category
                    {
                        Name = model.Category.Name
                    };
                    await dbContext.Categories.AddAsync(categoryEntity);
                }
                else
                    categoryEntity = byNameQuery;
            }

            entity.Name = model.Name;
            entity.Category = categoryEntity;
            entity.IsActive = model.IsActive;
            entity.Price = model.Price;

            await dbContext.SaveChangesAsync();
        }
    }
}
