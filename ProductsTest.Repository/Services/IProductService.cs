﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProductsTest.Repository.DTO;

namespace ProductsTest.Repository.Services
{
    public interface IProductService
    {
        Task<IList<ProductModel>> GetAllAsync();
        Task CreateOrUpdateAsync(ProductModel model);
        Task<ProductModel> GetAsync(int id);
    }
}