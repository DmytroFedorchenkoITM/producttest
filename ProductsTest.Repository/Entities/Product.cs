﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProductsTest.Repository.Entities
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public virtual Category Category { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [Range(0, 1000000)]
        public decimal Price { get; set; }
    }
}
