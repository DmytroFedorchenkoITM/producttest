﻿using Microsoft.EntityFrameworkCore;
using ProductsTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsTest.Repository
{
    public class ProductTestDbContext : DbContext
    {
        public ProductTestDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }
    }
}
