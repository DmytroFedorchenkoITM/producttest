﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsTest.Repository.DTO.Validators
{
    class ProductValidator : AbstractValidator<ProductModel>
    {
        public ProductValidator()
        {
            RuleFor(p => p.Name).NotEmpty();

            RuleFor(p => p.Category.Name).NotEmpty();

            RuleFor(p => p.Price).GreaterThanOrEqualTo(0M);
        }
    }
}
