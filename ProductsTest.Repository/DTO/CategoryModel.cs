﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsTest.Repository.DTO
{
    public class CategoryModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
