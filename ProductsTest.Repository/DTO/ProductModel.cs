﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsTest.Repository.DTO
{
    public class ProductModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual CategoryModel Category { get; set; }

        public bool IsActive { get; set; }

        public decimal Price { get; set; }

        public static ProductModel New() => new ProductModel
        {
            Name = "",
            Category = new CategoryModel
            {
                Name = ""
            },
            IsActive = true,
            Price = 0M
        };
    }
}
