﻿using AutoMapper;
using ProductsTest.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductsTest.Repository.DTO.Map
{
    public class ProductMapping : Profile
    {
        public ProductMapping()
        {
            CreateMap<Product, ProductModel>();

            CreateMap<Category, CategoryModel>();
        }
    }
}
