﻿using Microsoft.AspNetCore.Mvc;
using ProductsTest.Repository.DTO;
using ProductsTest.Repository.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductsTest.Controllers
{
    
    public class ProductsController : Controller
    {
        private readonly IProductService productService;

        public ProductsController(IProductService productService)
        {
            this.productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var products = await productService.GetAllAsync();
            if (!string.IsNullOrEmpty(TempData["SavedMessage"]?.ToString()))
                ViewBag.Message = TempData["SavedMessage"].ToString();
            return View(products);
        }

        [HttpGet]
        [Route("Edit")]
        [Route("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var product = await productService.GetAsync(id);
            return View(product ?? ProductModel.New());
        }

        [HttpPost]
        public async Task<IActionResult> Save(ProductModel model)
        {
            await productService.CreateOrUpdateAsync(model);
            TempData["SavedMessage"] = "Product was saved";
            return RedirectToAction("Index");
        }
    }
}
